import unittest

def analyze_text():
    pass

class TextAnalysisTests(unittest.TestCase):
    """ Tests for the ``analyze_text`` function."""

    def setUp(self):
        self.filename = 'text_analysis_test_file.txt'
        with open(self.filename, 'w') as f:
            f.write('fdfdf \n')

    def test_function_runs(self):
        """Basic smoke test: does the function run."""
        analyze_text()


if __name__ == '__main__':
    unittest.main()
