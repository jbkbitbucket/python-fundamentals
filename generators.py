def gen123():
    yield 1
    yield 2
    yield 3


g = gen123()
print(type(g))


def printg():
    try:
        print(next(g))
        print(next(g))
        print(next(g))
        print(next(g))
    except StopIteration:
        print('Stop Iteration error')


printg()
