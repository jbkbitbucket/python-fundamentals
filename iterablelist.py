from math import factorial as fac

args = "As you’ve seen, variables store strings and integer values".split()
print(args)

print([len(w) for w in args])


les = []
for l in args:
    les.append(len(l))

print(les)

f = [len(str(fac(x))) for x in range(20)]
print(type(f))
print(f)
